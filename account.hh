/** \brief OlmAccount wrapper
 * \file
 * \author Hubert Chathi <hubert@uhoreg.ca>
 * \copyright 2018 Hubert Chathi
 * \copyright
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * \copyright
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef __OLM_NAN_ACCOUNT_HH_
#define __OLM_NAN_ACCOUNT_HH_

#include "olm/olm.h"

#include <nan.h>

namespace olm_nan {

  class AccountWrap : public Nan::ObjectWrap {
  public:
    static NAN_MODULE_INIT(Init) {
      v8::Local<v8::FunctionTemplate> tpl = Nan::New<v8::FunctionTemplate>(New);
      tpl->SetClassName(Nan::New("Account").ToLocalChecked());
      tpl->InstanceTemplate()->SetInternalFieldCount(1);

      Nan::SetPrototypeMethod(tpl, "free", free);
      Nan::SetPrototypeMethod(tpl, "create", create);
      Nan::SetPrototypeMethod(tpl, "identity_keys", identity_keys);
      Nan::SetPrototypeMethod(tpl, "sign", sign);
      Nan::SetPrototypeMethod(tpl, "one_time_keys", one_time_keys);
      Nan::SetPrototypeMethod(tpl, "mark_keys_as_published", mark_keys_as_published);
      Nan::SetPrototypeMethod(tpl, "max_number_of_one_time_keys", max_number_of_one_time_keys);
      Nan::SetPrototypeMethod(tpl, "generate_one_time_keys", generate_one_time_keys);
      Nan::SetPrototypeMethod(tpl, "remove_one_time_keys", remove_one_time_keys);
      Nan::SetPrototypeMethod(tpl, "pickle", pickle);
      Nan::SetPrototypeMethod(tpl, "unpickle", unpickle);

      constructor().Reset(Nan::GetFunction(tpl).ToLocalChecked());
      Nan::Set(target, Nan::New("Account").ToLocalChecked(),
               Nan::GetFunction(tpl).ToLocalChecked());
    }

  private:
    friend class SessionWrap;
    void *buf;
    OlmAccount *ptr;

    AccountWrap();
    ~AccountWrap();

    static inline Nan::Persistent<v8::Function> & constructor() {
      static Nan::Persistent<v8::Function> my_constructor;
      return my_constructor;
    }

    static NAN_METHOD(New);
    static NAN_METHOD(free);
    static NAN_METHOD(create);
    static NAN_METHOD(identity_keys);
    static NAN_METHOD(sign);
    static NAN_METHOD(one_time_keys);
    static NAN_METHOD(mark_keys_as_published);
    static NAN_METHOD(max_number_of_one_time_keys);
    static NAN_METHOD(generate_one_time_keys);
    static NAN_METHOD(remove_one_time_keys);
    static NAN_METHOD(pickle);
    static NAN_METHOD(unpickle);
  };
}

#endif // __OLM_NAN_ACCOUNT_HH_
