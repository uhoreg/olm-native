/** \brief utility functions
 * \file
 * \author Hubert Chathi <hubert@uhoreg.ca>
 * \copyright 2018 Hubert Chathi
 * \copyright
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * \copyright
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef __OLM_NAN_UTIL_HH_
#define __OLM_NAN_UTIL_HH_

#include <nan.h>
#include <cstdlib>
#include <memory>

#define RETURN(x) info.GetReturnValue().Set(x);
#define RETURN_STR(ptr,len) RETURN(Nan::New(ptr, len).ToLocalChecked())

namespace olm_nan {
  /** Buffer for storing character data.
   * Zeroes the memory when it's freed, so that sensitive data does not linger
   * around in memory.
   */
  class Buffer {
    std::size_t _length;
    std::unique_ptr<char[]> buf;
  public:
    Buffer(std::size_t length)
      : _length(length),
        buf(new char[length]) {}
    Buffer(std::unique_ptr<char[]> &&_buf, std::size_t length)
      : _length(length),
        buf(std::move(_buf)) {}
    Buffer(Buffer &&other)
      : _length(other._length),
        buf(std::move(other.buf)) {}
    ~Buffer();
    char * get() { return buf.get(); }
    std::size_t length() { return _length; }
  };

  Buffer get_random(std::size_t len);
  Buffer buffer_from_v8_string(const v8::Local<v8::String> &string, Nan::Encoding encoding = Nan::ASCII);
}

#endif // __OLM_NAN_UTIL_HH_
