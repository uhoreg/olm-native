/** \brief OlmAccount wrapper
 * \file
 * \author Hubert Chathi <hubert@uhoreg.ca>
 * \copyright 2018 Hubert Chathi
 * \copyright
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * \copyright
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "account.hh"
#include "session.hh"
#include "util.hh"

#include <cstdlib>
#include <string>

using namespace olm_nan;

AccountWrap::AccountWrap()
  : buf(std::malloc(olm_account_size())),
    ptr(olm_account(buf)) {}

AccountWrap::~AccountWrap() {
  if (ptr) {
    olm_clear_account(ptr);
    std::free(buf);
  }
}

#define CHK_ACCOUNT_ERR(x)                        \
  if (x == olm_error()) {                          \
    std::string error = "OLM.";                   \
    error += olm_account_last_error(account);     \
    Nan::ThrowError(error.c_str());               \
    return;                                       \
  }

NAN_METHOD(AccountWrap::New) {
  if (info.IsConstructCall()) {
    AccountWrap *wrap = new AccountWrap();
    wrap->Wrap(info.This());
    RETURN(info.This());
  } else {
    const int argc = 0;
    v8::Local<v8::Value> argv[argc] = {};
    v8::Local<v8::Function> cons = Nan::New(constructor());
    RETURN(Nan::NewInstance(cons, argc, argv).ToLocalChecked());
  }
}

NAN_METHOD(AccountWrap::free) {
  AccountWrap* wrap = Nan::ObjectWrap::Unwrap<AccountWrap>(info.Holder());
  if (wrap->ptr) {
    olm_clear_account(wrap->ptr);
    std::free(wrap->buf);
    wrap->ptr = nullptr;
    wrap->buf = nullptr;
  }
}

NAN_METHOD(AccountWrap::create) {
  OlmAccount* account = Nan::ObjectWrap::Unwrap<AccountWrap>(info.Holder())->ptr;
  std::size_t random_length = olm_create_account_random_length(account);
  Buffer random_buffer(get_random(random_length));
  CHK_ACCOUNT_ERR(olm_create_account(
    account, random_buffer.get(), random_length
  ));
}

NAN_METHOD(AccountWrap::identity_keys) {
  OlmAccount* account = Nan::ObjectWrap::Unwrap<AccountWrap>(info.Holder())->ptr;
  size_t keys_length = olm_account_identity_keys_length(account);
  Buffer keys(keys_length);
  CHK_ACCOUNT_ERR(olm_account_identity_keys(
    account, keys.get(), keys_length
  ));
  RETURN_STR(keys.get(), keys_length);
}

NAN_METHOD(AccountWrap::sign) {
  OlmAccount* account = Nan::ObjectWrap::Unwrap<AccountWrap>(info.Holder())->ptr;
  size_t signature_length = olm_account_signature_length(account);
  Buffer message_buffer = buffer_from_v8_string(Nan::To<v8::String>(info[0]).ToLocalChecked());
  Buffer signature_buffer(signature_length);
  CHK_ACCOUNT_ERR(olm_account_sign(
    account,
    message_buffer.get(), message_buffer.length(),
    signature_buffer.get(), signature_length
  ));
  RETURN_STR(signature_buffer.get(), signature_length);
}

NAN_METHOD(AccountWrap::one_time_keys) {
  OlmAccount* account = Nan::ObjectWrap::Unwrap<AccountWrap>(info.Holder())->ptr;
  size_t keys_length = olm_account_one_time_keys_length(account);
  Buffer keys(keys_length);
  CHK_ACCOUNT_ERR(olm_account_one_time_keys(
    account, keys.get(), keys_length
  ));
  RETURN_STR(keys.get(), keys_length);
}

NAN_METHOD(AccountWrap::mark_keys_as_published) {
  OlmAccount* account = Nan::ObjectWrap::Unwrap<AccountWrap>(info.Holder())->ptr;
  CHK_ACCOUNT_ERR(olm_account_mark_keys_as_published(account));
}

NAN_METHOD(AccountWrap::max_number_of_one_time_keys) {
  OlmAccount* account = Nan::ObjectWrap::Unwrap<AccountWrap>(info.Holder())->ptr;
  RETURN((uint32_t) olm_account_max_number_of_one_time_keys(account));
}

NAN_METHOD(AccountWrap::generate_one_time_keys) {
  OlmAccount* account = Nan::ObjectWrap::Unwrap<AccountWrap>(info.Holder())->ptr;
  uint32_t number_of_keys = Nan::To<uint32_t>(info[0]).FromMaybe(0);
  size_t random_length = olm_account_generate_one_time_keys_random_length(account, number_of_keys);
  Buffer random(get_random(random_length));
  CHK_ACCOUNT_ERR(olm_account_generate_one_time_keys(
    account, number_of_keys, random.get(), random_length
  ));
}

NAN_METHOD(AccountWrap::remove_one_time_keys) {
  OlmAccount* account = Nan::ObjectWrap::Unwrap<AccountWrap>(info.Holder())->ptr;
  OlmSession* session = Nan::ObjectWrap::Unwrap<SessionWrap>(Nan::To<v8::Object>(info[0]).ToLocalChecked())->ptr;
  CHK_ACCOUNT_ERR(olm_remove_one_time_keys(account, session));
}

NAN_METHOD(AccountWrap::pickle) {
  OlmAccount* account = Nan::ObjectWrap::Unwrap<AccountWrap>(info.Holder())->ptr;
  size_t pickle_length = olm_pickle_account_length(account);
  Buffer key_buffer = buffer_from_v8_string(Nan::To<v8::String>(info[0]).ToLocalChecked());
  Buffer pickle_buffer(pickle_length);
  CHK_ACCOUNT_ERR(olm_pickle_account(
    account, key_buffer.get(), key_buffer.length(), pickle_buffer.get(), pickle_length
  ));
  RETURN_STR(pickle_buffer.get(), pickle_length);
}

NAN_METHOD(AccountWrap::unpickle) {
  OlmAccount* account = Nan::ObjectWrap::Unwrap<AccountWrap>(info.Holder())->ptr;
  Buffer key_buffer = buffer_from_v8_string(Nan::To<v8::String>(info[0]).ToLocalChecked());
  Buffer pickle_buffer = buffer_from_v8_string(Nan::To<v8::String>(info[1]).ToLocalChecked());
  CHK_ACCOUNT_ERR(olm_unpickle_account(
    account, key_buffer.get(), key_buffer.length(),
    pickle_buffer.get(), pickle_buffer.length()
  ));
}
