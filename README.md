# Native bindings for Olm

[Olm](https://git.matrix.org/git/olm/) is an implementation of the Double
Ratchet cryptographic ratchet described by
https://whispersystems.org/docs/specifications/doubleratchet/, written in C and
C++11 and exposed as a C API.

This package provides a Node.js bining using the native olm library, rather
than transpiling the library to JavaScript using emscripten.  This library
is compatible with the JavaScript library provided by Olm.

## Installing

You need to have the Olm library installed.  You can do this by either
downloading Olm from https://git.matrix.org/git/olm/ and running `make
install`, or if your distribution provides a package, you can install it using
your package manager.  If you are installing from your package manager, make
sure to install the development package.

After Olm is installed, then you can run `npm install olm-native`.

# Copyright and license

Copyright 2018 Hubert Chathi <hubert@uhoreg.ca>

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
